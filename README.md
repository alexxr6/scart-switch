/!\ PROJECT NOT TESTED (I didn't had time to buy all elements to test it)/!\
This project is about a scalable SCART switch which can handle up to 64 INPUTS.
This switch is based on a master board, which contains an Arduino Nano to manage all ports, and extension boards.
Each board have 8 inputs ports (and 1 output for the master).
The ports selection is made by two switches (Previous and Next) or an IR remote.
I designed the PCB to be populated as an master or extension port (you do not need to print 5 master boards and only use one)
